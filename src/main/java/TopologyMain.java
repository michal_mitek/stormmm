import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.topology.TopologyBuilder;

public class TopologyMain {

    public static void main(String[] args) throws InterruptedException {

        TopologyBuilder builder = new TopologyBuilder();
        builder.setSpout("Read-Fields-Spout", new ReadFilesSpout());
        builder.setBolt("Filter-Fields-To-File-Bolt", new FilterFieldsToFileBolt()).shuffleGrouping("Read-Fields-Spout");

        Config config = new Config();
        config.setDebug(true);
        config.put("fileToRead", "C:\\Users\\mitek\\Desktop\\SourceCode1-11\\stormTut\\src\\main\\resources\\fields.txt");
        config.put("dirToWrite", "C:\\Users\\mitek\\Desktop\\SourceCode1-11\\stormTut\\src\\main\\resources\\newDir");

        LocalCluster cluster = new LocalCluster();
        try {
            cluster.submitTopology("Write-To-File-topology", config, builder.createTopology());
            Thread.sleep(1000);
        }
        finally {
            cluster.shutdown();
        }
    }

}
